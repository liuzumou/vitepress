# 大模型基础

## 概念名词
### 大语言模型（Large Language Model，简称 LLM）

是指一种基于深度学习的自然语言处理（NLP）模型，通常具有数十亿甚至数千亿的参数规模。这些模型通过大量的文本数据进行训练，能够理解和生成人类语言，广泛应用于文本生成、翻译、问答、对话等任务

### 生成式 AI（Artificial Intelligence Generated Content，AIGC）

生成式 AI 是指一类能够生成新内容的人工智能技术。它通过学习数据的分布规律，生成与训练数据相似但全新的内容。生成的内容可以是文本、图像、音频、视频等。

### 检索增强生成RAG（Retrieval Augmented Generation）

RAG是一种结合了信息检索和文本生成的技术，能够在大模型生成答案时利用外部知识库中的相关信息。

## 常用模型

## 开发框架/工具
### LangChain

是一个用于构建基于大语言模型（LLM）应用的框架。它提供了丰富的工具和模块，帮助开发者轻松地将 LLM 集成到应用程序中，实现诸如问答系统、文本生成、对话机器人等功能

## 实践&技巧
RAG效果优化：https://help.aliyun.com/zh/model-studio/use-cases/rag-optimization?spm=a2c4g.11186623.help-menu-2400256.d_2_2.242e71eeF5a4QS
