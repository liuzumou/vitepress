# git 设置本地代理

默认情况下，git是无法连接到github站点的，需要设置代理才能访问。

## 查看代理端口
打开代理软件，查看代理端口。clash verge 的代理端口可以在设置➡️Clash设置中查看。默认为7897

## 设置全局代理
git config --global http.proxy http://127.0.0.1:7897
git config --global https.proxy http://127.0.0.1:7897

## 取消全局代理
git config --global --unset http.proxy
git config --global --unset https.proxy