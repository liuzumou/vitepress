# elasticsearch 使用
## 1. 索引

```shell
# 创建索引
PUT /indexName
# 查询索引信息
GET /indexName
# 删除索引
DELETE /indexName
```

## 2.文档

```shell
# 创建文档
POST /indexName/_doc/id
{
	field1:value1,
	field2:value2,
	field3:{
		field11:value11,
		field22:value22
	}
}
# 查询单个文档
GET /indexName/_doc/id
# 查询文档列表
GET /indexName/_search

# 全量修改文档 --id不存在时则新增
PUT /indexName/_doc/id

# 增量修改，只修改匹配的字段
POST /indexName/_update/id

# 删除文档
DELETE /indexName/_doc/id
```

