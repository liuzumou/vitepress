# opengauss 安装和使用

## 拉取镜像
仓库地址：https://hub.docker.com/r/enmotech/opengauss
```bash
docker pull enmotech/opengauss:6.0.0
```
## 运行容器
```bash
docker run --name opengauss --privileged=true -d -e GS_PASSWORD=openGauss@123 -p 26000:5432 -v /docker_files/opengauss:/var/lib/opengauss/data enmotech/opengauss:6.0.0
```

## 进入容器
```bash
docker exec -it opengauss bash
```

## 数据库操作
初始时，openGauss包含两个模板数据库template0、template1，以及一个默认的用户数据库postgres。

## gsql
openGauss提供了gsql工具，用于数据库操作。

gsql -d dbname -h localhost -p port -U user -W password -r

gsql 选项：

-d选项： 指定gsql客户端连接的数据库

-h选项： 指定gsql客户端连接的服务器IP

-U选项： 指定gsql客户端连接数据库的用户名

-p选项： 指定gsql客户端连接的服务器端口号

-W选项： 指定gsql客户端连接的用户密码

-E选项： gsql客户端程序执行元命令的时候，显示其对应的SQL语句

**-r选项：** gsql客户端程序执行元命令的时候，使用上下箭头以以及解决backspace乱码问题。

```bash
# 默认是root用户，需切换到omm用户
su - omm
# 进入gsql
gsql
# gsql 更多参数  -r 可以使用上下箭头以及backspace
gsql -d dbname -h localhost -p port -U user -W password -r
```
## 数据库操作
```bash
# 查看数据库列表
\l
# 创建数据库
# DBCOMPATIBILITY 取值范围：A、B、C、PG。分别表示兼容O、MY、TD和POSTGRES；默认为PG。
# --创建兼容PG格式的数据库。
CREATE DATABASE pg_compatible_db DBCOMPATIBILITY 'PG';
# --创建兼容ORA格式的数据库。
CREATE DATABASE ora_compatible_db DBCOMPATIBILITY 'A';
# --删除数据库
DROP DATABASE dbname;

# 进入\切换数据库
\c dbname
# 查看数据库表信息
\d
# 查询表
select * from tablename;
# 退出gsql
\q
```

## 相关资料
1. [openGauss官网](https://opengauss.org/zh/)
