# docker 安装redis

## 创建配置文件夹和配置文件

```shell
# 创建文件夹
mkdir redis
# 新建配置文件
vi redis/redis.conf
requirepass your-password
```

## 创建容器

```shell
docker run -d --name myredis  --restart=always  -p 56379:6379 -v /root/docker/redis:/usr/local/etc/redis redis redis-server /usr/local/etc/redis/redis.conf
```

## 查看版本

```shell
# 进入容器
docker exec -it myredis bash
# 查看版本
redis-cli --version
# 进入cli
redis-cli
# 退出cli
exit
```

